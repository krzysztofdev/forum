<?php

namespace App\Controller;

class Administrator extends Controller {

    public function panel() {
        (new \App\View\Administrator)->panel();
    }

    public function users() {
        $perPage = 5;
        (new \App\View\Administrator)->users([
            'pagination' => [
                'perPage' => $perPage,
                'currentPage' => $_GET['page'] ?? 1,
                'items' => (new \App\Model\User)->count()
            ],
            'users' => (new \App\Model\Administrator)->users($_GET['page'] ?? 1, $perPage)
        ]);
    }

    public function categories() {
        (new \App\View\Administrator)->categories([
            'categories' => (new \App\Model\Category)->hierarchicCatList()
        ]);
    }

}