<?php

namespace App\Controller;

class Category extends Controller {

    public function list() {
        (new \App\View\Category())->list([
            'data' => (new \App\Model\Category())->categories($this->id ?? null)
        ]);
    }

    public function add() {
        if(!empty($this->data)) {
            if((new \App\Model\Category)->insert($this->data, ['name','parent_id']))
                (new \App\Model\Alert())->addAlert('success', 'ADDED_SUCCESSFULLY');
            else
                (new \App\Model\Alert())->addAlert('danger', 'ERROR_OCCURRED');
            $this->redirectBack();
        }
        (new \App\View\Category)->add([
            'categories' => (new \App\Model\Category)->hierarchicCatList()
        ]);
    }

    public function edit() {
        $category = new \App\Model\Category;
        if(!empty($this->data)) {
            if($category->update($this->id, $this->data, ['name', 'parent_id', 'id']))
                (new \App\Model\Alert())->addAlert('success', 'EDITED_SUCCESSFULLY');
            else
                (new \App\Model\Alert())->addAlert('danger', 'ERROR_OCCURRED');
            $this->redirectBack();
        }
        (new \App\View\Category)->edit([
            'category' => $category->get($this->id),
            'categories' => $category->hierarchicCatList()
        ]);
    }

    public function remove() {
        if((new \App\Model\Category)->delete($this->id))
            (new \App\Model\Alert())->addAlert('info', 'DELETED_SUCCESSFULLY');
        else
            (new \App\Model\Alert())->addAlert('danger', 'ERROR_OCCURRED');
        $this->redirectBack();
    }

}
