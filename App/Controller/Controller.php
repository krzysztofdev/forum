<?php

namespace App\Controller;

use App\Core\AntiCSRF;

abstract class Controller {

    protected $id, $data;

    public function __construct() {
        AntiCSRF::generateToken();
        $this->id = $_GET['id'] ?? null;
        $this->data = $_POST['data'] ?? null;
    }

    protected function redirectBack() {
        empty($_SERVER["HTTP_REFERER"])? $this->redirectHome() : $this->doRedirection($_SERVER["HTTP_REFERER"]);
    }

    protected function redirectHome() {
        $this->doRedirection(dirname($_SERVER['PHP_SELF']).'/');
    }

    private function doRedirection($url) {
        header('Location: '. $url);
        exit('<a href="' . $url . '">Click here</a>.');
    }

    protected function redirectTo($controller, $method, $id = null) {
        $this->doRedirection(\App\View\View::urlGenerator($controller, $method, $id));
    }

}
