<?php

namespace App\Controller;

class Group extends Controller {

    public function list() {
        (new \App\View\Group)->list([
            'groups' => (new \App\Model\Group)->list()
        ]);
    }

    public function edit() {
        if(!empty($this->data)) {
            if((new \App\Model\Group)->update($this->id, $this->data, ['name']))
                (new \App\Model\Alert())->addAlert('success', 'EDITED_SUCCESSFULLY');
            else
                (new \App\Model\Alert())->addAlert('danger', 'ERROR_OCCURRED');
            $this->redirectBack();
        }
        (new \App\View\Group)->edit([
            'group' => (new \App\Model\Group)->get($this->id)
        ]);
    }

    public function add() {
        if(!empty($this->data)) {
            if((new \App\Model\Group)->insert($this->data, ['name']))
                (new \App\Model\Alert())->addAlert('success', 'ADDED_SUCCESSFULLY');
            else
                (new \App\Model\Alert())->addAlert('danger', 'ERROR_OCCURRED');
            $this->redirectBack();
        }
        (new \App\View\Group)->add();
    }

    public function remove() {
        if(!empty($this->id)) {
            if((new \App\Model\Route)->revokeGroup($this->id) && (new \App\Model\Group)->delete($this->id))
                (new \App\Model\Alert())->addAlert('info', 'DELETED_SUCCESSFULLY');
            else
                (new \App\Model\Alert())->addAlert('danger', 'ERROR_OCCURRED');
            $this->redirectBack();
        }
    }
}