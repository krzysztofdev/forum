<?php

namespace App\Controller;

class Page extends Controller {

    public function notFound() {
        http_response_code(404);
        (new \App\View\Page)->notFound();
    }

    public function forbidden() {
        http_response_code(403);
        (new \App\View\Page)->forbidden();
    }

    public function internalError() {
        http_response_code(500);
        (new \App\View\Page)->internalError();
    }

}