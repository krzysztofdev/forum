<?php

namespace App\Controller;

class Post extends Controller {

    public function new() {
        if (empty($this->id))
            $this->redirectHome();
        if (!empty($this->data)) {
            if ((new \App\Model\Post())->insert(array_merge($this->data, ['thread_id' => $this->id]), ['thread_id', 'user_id', 'content'])) {
                (new \App\Model\Alert())->addAlert('success', 'ADDED_SUCCESSFULLY');
                $this->redirectTo('thread', 'show', $this->id);
            } else {
                (new \App\Model\Alert())->addAlert('danger', 'ERROR_OCCURRED');
                $this->redirectBack();
            }
        } else {
            $thread = (new \App\Model\Thread())->get($this->id);
            (new \App\View\Post())->new([
                'thread' => $thread
            ]);
        }
    }

    public function edit() {
        if((new \App\Model\Post)->get($this->id)['user_id'] == \App\Core\User::user()->getID())
            $this->manage();
        else
            $this->redirectBack();
    }

    public function manage() {
        if(empty($this->id))
            $this->redirectHome();
        $post = (new \App\Model\Post)->get($this->id);
        if (!empty($this->data)) {
            if((new \App\Model\Post)->update($this->id, $this->data, ['content'])) {
                (new \App\Model\Alert())->addAlert('success', 'EDITED_SUCCESSFULLY');
                $this->redirectTo('thread','show', $post['thread_id']);
            } else {
                (new \App\Model\Alert())->addAlert('danger', 'ERROR_OCCURRED');
                $this->redirectBack();
            }
        } else {
            $thread = (new \App\Model\Thread)->get($post['thread_id']);
            (new \App\View\Post)->edit([
                'post' => $post,
                'thread' => $thread
            ]);
        }
    }

    public function remove() {
        if((new \App\Model\Post)->get($this->id)['user_id'] == \App\Core\User::user()->getID())
            $this->delete();
        $this->redirectBack();
    }

    public function delete() {
        if(empty($this->id))
            $this->redirectHome();
        if((new \App\Model\Post)->delete($this->id))
            (new \App\Model\Alert())->addAlert('info', 'DELETED_SUCCESSFULLY');
        else
            (new \App\Model\Alert())->addAlert('danger', 'ERROR_OCCURRED');
        $this->redirectBack();
    }

}
