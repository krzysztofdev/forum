<?php


namespace App\Controller;

use App\Model\Alert;

class Route extends Controller {

    public function list() {
        (new \App\View\Route())->list([
            'routes' => (new \App\Model\Route())->list()
        ]);
    }

    public function add() {
        if(!empty($this->data)) {
            if((new \App\Model\Route)->insert($this->data, ['controller', 'method']))
                (new Alert())->addAlert('success', 'ADDED_SUCCESSFULLY');
            else
                (new Alert())->addAlert('danger', 'ERROR_OCCURRED');
            $this->redirectBack();
        }
        (new \App\View\Route())->add();
    }

    public function remove() {
        if((new \App\Model\Route)->delete($this->id))
            (new Alert())->addAlert('success', 'DELETED_SUCCESSFULLY');
        else
            (new Alert())->addAlert('danger', 'ERROR_OCCURRED');
        $this->redirectBack();
    }

    public function permissions() {
        $groups = (new \App\Model\Group)->list();
        if(isset($_POST['submit'])) {
            (new \App\Model\Route)->allow($this->id, array_keys($this->data));
            $this->redirectBack();
        }
        $route = (new \App\Model\Route);
        (new \App\View\Route())->permissions([
            'allowedGroups' => $route->allowedGroups($this->id),
            'groups' => $groups,
            'route' => $route->get($_GET['id'])
        ]);
    }

}