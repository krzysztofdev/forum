<?php

namespace App\Controller;

class Thread extends Controller {

    public function list() {
        $perPage = 10;
        $page = $_GET['page'] ?? 1;
        $model = new \App\Model\Thread;
        $threads = $model->threads($this->id, $page, $perPage);
        (new \App\View\Thread)->list([
            'categories' => ($page==1 ? (new \App\Model\Category())->categories($this->id) : null),
            'pagination' => [
                'perPage' => $perPage,
                'currentPage' => $page,
                'items' => $model->countThreads($this->id)
            ],
            'threads' => $threads
        ]);
    }

    public function show() {
        $perPage = 5;
        $model = new \App\Model\Thread;
        (new \App\View\Thread)->show([
            'thread' => $model->get($this->id ?? $this->redirectHome()),
            'pagination' => [
                'perPage' => $perPage,
                'currentPage' => $_GET['page'] ?? 1,
                'items' => $model->countPosts($this->id)
            ],
            'posts' => $model->getPosts($this->id, $_GET['page'] ?? 1, $perPage)
        ]);
    }

    public function new() {
        if (!empty($this->id)) {
            if (!empty($this->data)) {
                $new = (new \App\Model\Thread())->add($this->data);
                if (!empty($new)) {
                    (new \App\Model\Alert())->addAlert('success', 'ADDED_SUCCESSFULLY');
                    $this->redirectTo('thread', 'show', $new);
                } else {
                    (new \App\Model\Alert())->addAlert('danger', 'ERROR_OCCURRED');
                    $this->redirectBack();
                }
            } else {
                (new \App\View\Thread())->new();
            }
        } else {
            $this->redirectHome();
        }
    }

    public function delete() {
        if (!empty($this->id)) {
            if (!empty((new \App\Model\Thread())->delete($this->id))) {
                (new \App\Model\Alert())->addAlert('info', 'DELETED_SUCCESSFULLY');
                $this->redirectHome();
            } else {
                (new \App\Model\Alert())->addAlert('danger', 'ERROR_OCCURRED');
                $this->redirectBack();
            }
        }
    }

    public function remove() {
        if((new \App\Model\Thread)->get($this->id)['user_id'])
            $this->delete();
        $this->redirectBack();
    }

    public function edit() {
        if((new \App\Model\Thread)->get($this->id)['user_id'] == \App\Core\User::user()->getID())
            $this->manage();
        else
            $this->redirectBack();
    }

    public function manage() {
        if(!empty($this->data)) {
            if((new \App\Model\Thread)->update($this->id, $this->data, ['category_id', 'name'])) {
                (new \App\Model\Alert())->addAlert('success', 'EDITED_SUCCESSFULLY');
                $this->redirectTo('thread', 'show', $this->id);
            } else {
                (new \App\Model\Alert())->addAlert('danger', 'ERROR_OCCURRED');
                $this->redirectBack();
            }
        }
        (new \App\View\Thread())->edit([
            'cats' => (new \App\Model\Category)->hierarchicCatList(),
            'thread' => (new \App\Model\Thread)->get($this->id)
        ]);
    }
}
