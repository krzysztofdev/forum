<?php

namespace App\Controller;

use App\Model\Alert;

class User extends Controller {

    public function login() {
        if (!empty($_SESSION['user']) || (!empty($this->data) && (new \App\Model\User())->login($this->data))) {
            (new Alert())->addAlert('success', 'LOGGED_IN_SUCCESSFULLY');
            $this->redirectHome();
        }
        (new \App\View\User())->login();
    }

    public function logout() {
        if (!empty($_SESSION['user'])) {
            session_destroy();
            (new Alert())->addAlert('success', 'LOGGED_OUT_SUCCESSFULLY');
        }
        $this->redirectHome();
    }

    public function panel() {
        (new \App\View\User())->panel();
    }

    public function edit() {
        if(!empty($this->data) && $this->validateUserData($this->data, \App\Core\User::user()->getID())) {
            if((new \App\Model\User)->edit($_SESSION['user'], $this->data)) {
                (new Alert())->addAlert('success', 'EDITED_SUCCESSFULLY');
                $this->redirectHome();
            } else {
                (new Alert())->addAlert('danger', 'ERROR_OCCURRED');
                $this->redirectBack();
            }
        } else
        (new \App\View\User())->edit([
            'user' => (new \App\Model\User)->get($_SESSION['user'])
        ]);
    }

    public function delete() {
        if((new \App\Model\User())->delete($this->id))
            (new Alert())->addAlert('info', 'DELETED_SUCCESSFULLY');
        else
            (new Alert())->addAlert('danger', 'ERROR_OCCURRED');
        $this->redirectBack();
    }

    public function remove() {
        if((new \App\Model\User())->delete(\App\Core\User::user()->getID()))
            $this->logout();
        else
            (new Alert())->addAlert('danger', 'ERROR_OCCURRED');
        $this->redirectBack();
    }

    public function manage() {
        if(!empty($this->data) && $this->validateUserData($this->data, $this->id)) {
            if(!empty($this->data['password']) && ($this->data['password'] != $this->data['password2']))
                (new Alert())->addAlert('danger', 'DIFFERENT_PASSWORDS');
            else {
                if((new \App\Model\User)->update($this->id, $this->data, ['name', 'email', 'group_id', 'password']))
                    (new Alert())->addAlert('success', 'EDITED_SUCCESSFULLY');
                else
                    (new Alert())->addAlert('danger', 'ERROR_OCCURRED');
                $this->redirectBack();
            }
        }
        (new \App\View\Administrator)->user([
            'user' => (new \App\Model\User)->get($this->id),
            'groups' => (new \App\Model\Group)->list()
        ]);
    }

    public function register() {
        if (!empty($this->data) && $this->validateUserData($this->data))
            if(!empty($_POST['password']) && ($_POST['password'] != $_POST['password2']))
                (new Alert())->addAlert('danger', 'DIFFERENT_PASSWORDS');
            else
                if((new \App\Model\User)->insert($this->data, ['name', 'email', 'password', 'group_id']))
                    (new Alert())->addAlert('success', 'USER_ADDED');
                else
                    (new Alert())->addAlert('danger', 'ERROR_OCCURRED');
        (new \App\View\User())->register();
    }


    private function validateUserData($data, $id = false) {
        $user = new \App\Model\User();
        if(!ctype_alnum($data['name']))
            (new Alert())->addAlert('danger', 'INVALID_LOGIN');
        elseif(!filter_var($data['email'], FILTER_VALIDATE_EMAIL))
            (new Alert())->addAlert('danger', 'INVALID_EMAIL');
        elseif(!in_array($user->userNameExists($data['name']), [$id, false]))
            (new Alert())->addAlert('danger', 'LOGIN_EXISTS');
        elseif(!in_array($user->emailExists($data['email']), [$id, false]))
            (new Alert())->addAlert('danger', 'EMAIL_EXISTS');
        else return true;
        return false;
    }

}
