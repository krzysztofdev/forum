<?php

namespace App\Core;

class Alert {
    public static function getAlerts() {
        if (empty($_SESSION['alerts']))
            $_SESSION['alerts'] = [];
        $alerts = $_SESSION['alerts'];
        unset($_SESSION['alerts']);
        return $alerts;
    }

    public static function setAlerts($alerts) {
        $_SESSION['alerts'] = $alerts;
    }

}
