<?php

namespace App\Core;

class AntiCSRF {

    private static $token;

    public static function generateToken() {
        self::$token = $_SESSION['antiCSRF_token'] ?? null;
        try {
            $random = random_bytes(10);
        } catch (\Exception $e) {
            $random = rand();
        }
        $_SESSION['antiCSRF_token'] = sha1($random.microtime());
    }

    public static function getToken() {
        return $_SESSION['antiCSRF_token'];
    }

    public static function checkToken($token) {
        if(!($token === self::$token && self::checkReferer()))
            throw new \Exception('Anti CSRF protection');
    }

    private static function checkReferer() {
        return $_SERVER['SERVER_NAME'] == parse_url($_SERVER['HTTP_REFERER'] ?? null, PHP_URL_HOST);
    }
}