<?php

namespace App\Core;

class Auth {

    private static $instance;
    private $acl;

    private function __construct() {
        $this->getAccessList();
    }

    public static function auth() {
        if(empty(self::$instance))
            self::$instance = new Auth();
        return self::$instance;
    }

    public function checkAccess($controller, $method) {
        return in_array($method, $this->acl[$controller] ?? []);
    }

    private function getAccessList() {
        $query = DB::getConn()->prepare('SELECT r.* FROM allowed_routes ar LEFT JOIN routes r ON r.id = ar.route_id WHERE ar.group_id = :id');
        $query->execute(['id' => User::user()->getGroup()]);
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        foreach($result as $row)
            $this->acl[$row['controller']][] = $row['method'];
    }
}