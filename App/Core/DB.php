<?php

namespace App\Core;
use \PDO;

class DB {

    private static $conn;

    private function __construct() {}

    public static function getConn() {
        if (empty(self::$conn)) {
            self::$conn = new PDO('pgsql:host=' . DBHOST . ';dbname=' . DBNAME . ';port=' . DBPORT . ';options=\'--client_encoding=UTF8\'', DBUSER, DBPASS);
            self::$conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        }
        return self::$conn;
    }

}
