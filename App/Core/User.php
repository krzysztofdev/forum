<?php

namespace App\Core;

class User {

    private static $instance;
    private $id;
    private $user;

    private function __construct() {
        $this->id = $_SESSION['user'] ?? 0;
    }

    public static function user() {
        if(empty(self::$instance))
            self::$instance = new User();
        return self::$instance;
    }

    public function getID() {
        return $this->id;
    }

    public function getGroup() {
        return $this->get()['group_id'];
    }

    public function loggedIn() {
        return $this->id === 0;
    }

    private function get() {
        if(empty($this->user))
            if($this->id === 0)
                $this->user = ['group_id' => 0];
            else
                $this->user = (new \App\Model\User)->get($this->id);
        return $this->user;
    }

}