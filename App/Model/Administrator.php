<?php

namespace App\Model;

class Administrator extends Model {

    public function users($page, $perPage) {
        $query = $this->db->prepare('SELECT u.*, g.name AS "group" FROM users u LEFT JOIN groups g ON u.group_id = g.id ORDER BY u.id LIMIT :perPage OFFSET ((:page - 1) * :perPage)');
        $query->execute([
            ':page' => $page,
            ':perPage' => $perPage
        ]);
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

}