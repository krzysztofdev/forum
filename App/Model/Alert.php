<?php

namespace App\Model;

class Alert {

    private $alerts;

    public function __construct() {
        $this->alerts = \App\Core\Alert::getAlerts();
    }

    public function addAlert($type, $message) {
        $this->alerts[$type][] = $message;
    }

    public function getAlerts() {
        $alerts = $this->alerts;
        $this->alerts = [];
        return $alerts;
    }

    public function __destruct() {
        \App\Core\Alert::setAlerts($this->alerts);
    }

}
