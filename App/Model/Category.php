<?php

namespace App\Model;

class Category extends Model {

    public function __construct() {
        parent::__construct();
        $this->table = 'categories';
    }

    public function insert(array $data, array $acceptedKeys = []): bool {
        $data['parent_id'] = (!empty($data['parent_id']) ? $data['parent_id'] : null);
        return parent::insert($data, $acceptedKeys);
    }

    public function update(int $id, array $data, array $acceptedKeys = []) {
        $data['parent_id'] = (!empty($data['parent_id']) ? $data['parent_id'] : null);
        return parent::update($id, $data, $acceptedKeys);
    }

    public function categories($id) {
        $query = $this->db->prepare('select cat.*, posts.date "lastPostDate", users.name "lastPostUser" from (
select mc.*, max(posts.id) as "lastPost", count(distinct threads.id) "threads" from categories mc left join threads on threads.category_id in (with recursive cats (id) as (
  select     id
  from       categories
  where      id = mc.id
  union all
  select     c.id
  from       categories c
  inner join cats
          on c.parent_id = cats.id
)
select * from cats) left join posts on threads.id = posts.thread_id where mc.parent_id ' . (!empty($id) ? '= :parent_id' : 'is null') . ' group by mc.id
) cat left join posts on cat."lastPost" = posts.id left join users on users.id = posts.user_id');
        if (!empty($id))
            $query->bindValue(':parent_id', $id);
        $query->execute();
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function hierarchicCatList($id = null) {
        $query = $this->db->prepare('select id, name from categories where parent_id '.(empty($id)? 'is null' :  '= '.(int) $id));
        $query->execute();
        $result = $query->fetchAll(\PDO::FETCH_ASSOC);
        foreach($result as &$r)
            $r['subcats'] = $this->hierarchicCatList($r['id']);
        return  $result;
    }
}
