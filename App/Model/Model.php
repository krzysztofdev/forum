<?php

namespace App\Model;

use App\Core\AntiCSRF;

abstract class Model {

    protected $db;
    protected $table;

    public function __construct() {

        $this->db = \App\Core\DB::getConn();
        $this->table = lcfirst((new \ReflectionClass($this))->getShortName()).'s';
    }

    public function count() {
        return $this->db->query('SELECT count(*) FROM '.$this->table, \PDO::FETCH_COLUMN, 0)->fetch();
    }

    public function insert(array $data, array $acceptedKeys = []) : bool {
        AntiCSRF::checkToken($_REQUEST['xsrf'] ?? '');
        if(!empty($acceptedKeys))
            $data = array_intersect_key($data, array_flip($acceptedKeys));
        $query = $this->db->prepare('INSERT INTO '.$this->table.' ('.implode(', ', array_keys($data)).') VALUES (:'.implode(', :', array_keys($data)).')');
        foreach ($data as $key => $val) {
            $data[':'.$key] = $val;
            unset($data[$key]);
        }
        $query->execute($data);
        return !empty($query->rowCount());
    }

    public function update(int $id, array $data, array $acceptedKeys = []) {
        AntiCSRF::checkToken($_REQUEST['xsrf'] ?? '');
        if(!empty($acceptedKeys))
            $data = array_intersect_key($data, array_flip($acceptedKeys));
        $q = 'UPDATE '.$this->table.' SET';
        foreach(array_keys($data) as $key)
            $q .= ' '.$key.' = :'.$key.',';
        $q = rtrim($q, ',').' WHERE id = :id';
        $query = $this->db->prepare($q);
        $data['id'] = $id;
        foreach ($data as $key => $val) {
            $data[':'.$key] = $val;
            unset($data[$key]);
        }
        $query->execute($data);
        return !empty($query->rowCount());
    }

    public function delete(int $id) {
        AntiCSRF::checkToken($_REQUEST['xsrf'] ?? '');
        $query = $this->db->prepare('DELETE FROM '.$this->table.' WHERE id = :id');
        $query->execute([':id' => $id]);
        return !empty($query->rowCount());
    }

    public function get(int $id) {
        $query = $this->db->prepare('SELECT * FROM '.$this->table.' WHERE id = :id');
        $query->execute([':id' => $id]);
        return $query->fetch(\PDO::FETCH_ASSOC);
    }

    public function list(int $page = null, int $perPage = null) {
        return $this->db->query('SELECT * FROM '.$this->table.' ORDER BY id'.
            ((!empty($page) && !empty($perPage)) ? ' LIMIT '.($page-1)*$perPage.','.$perPage : '')
            , \PDO::FETCH_ASSOC)->fetchAll();
    }


    public function nl2br(string $string) {
        return str_replace(["\r\n", "\r", "\n"], '<br>', $string);
    }
}
