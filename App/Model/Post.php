<?php

namespace App\Model;

class Post extends Model {

    public function insert(array $data, array $acceptedKeys = []): bool {
        $data['user_id'] = $_SESSION['user'];
        $data['content'] = $this->nl2br(htmlspecialchars($data['content']));
        return parent::insert($data, $acceptedKeys);
    }

    public function update(int $id, array $data, array $acceptedKeys = []) {
        $data['content'] = $this->nl2br(htmlspecialchars($data['content']));
        return parent::update($id, $data, $acceptedKeys);
    }

}
