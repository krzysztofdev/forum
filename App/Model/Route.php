<?php

namespace App\Model;

class Route extends Model {

    public function allowedGroups($id) {
        $query = $this->db->prepare('select id from allowed_routes ar left join groups g on ar.group_id = g.id where ar.route_id = :id');
        $query->execute([':id' => $id]);
        return $query->fetchAll(\PDO::FETCH_COLUMN);
    }

    public function allow($id, $groups) {
        $this->db->exec('DELETE FROM allowed_routes WHERE route_id = '.intval($id));
        $q = 'insert into allowed_routes (route_id, group_id) values';
        foreach($groups as $key => $val) {
            $q .= ' (' . intval($id) . ', :' . $key . '),';
            $groups[':'.$key] = $val;
            unset($groups[$key]);
        }
        $q = substr($q, 0, -1);
        $query = $this->db->prepare($q);
        $query->execute($groups);
        return $query->rowCount();
    }

    public function revokeGroup($id) {
        $query = $this->db->prepare('DELETE FROM allowed_routes WHERE group_id = :group_id');
        $query->execute([':group_id' => $id]);
        return $query->rowCount();
    }

}