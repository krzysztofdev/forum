<?php

namespace App\Model;

class Thread extends Model {

    public function getPosts(int $id, int $page, int $perPage) {
        $query = $this->db->prepare('SELECT p.id, p.date, p.content, p.user_id, u.name "userName", g.name "groupName", u.date "joinDate", u.email AS "userEmail" FROM posts p
                LEFT JOIN users u ON p.user_id = u.id
                LEFT JOIN groups g ON u.group_id = g.id
                WHERE p.thread_id = :thread_id ORDER BY p.date LIMIT :perPage OFFSET ((:page - 1) * :perPage)');
        $query->execute([
            ':thread_id' => $id,
            ':perPage' => $perPage,
            ':page' => $page
        ]);
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function countPosts($id) {
        $query = $this->db->prepare('SELECT count(*) FROM posts where thread_id = :thread_id');
        $query->execute([':thread_id' => $id]);
        return $query->fetch(\PDO::FETCH_COLUMN);
    }

    public function countThreads($id) {
        $query = $this->db->prepare('SELECT count(*) FROM threads where category_id = :category_id');
        $query->execute([':category_id' => $id]);
        return $query->fetch(\PDO::FETCH_COLUMN);
    }

    public function insert(array $data, array $acceptedKeys = []): bool {
        $data['user_id'] = $_SESSION['user'];
        return parent::insert($data, $acceptedKeys);
    }

    public function add($data) {
        if(!$this->insert($data, ['category_id', 'user_id', 'name']))
            return false;
        $thread = $this->db->lastInsertId('threads_id_seq');
        if(!empty($thread))
            (new Post())->insert(array_merge($data, ['thread_id' => $thread]), ['thread_id', 'user_id', 'content']);
        return $thread;
    }

    public function threads($id, $page, $perPage) {
        $query = $this->db->prepare('select threads.*, users.name as "lastPostUser", posts.date as "lastPostDate", "postsCount" from threads
                left join (select max(id) id, thread_id, count(*) as "postsCount" from posts group by thread_id) p ON p.thread_id = threads.id
                left join posts on p.id = posts.id
                left join users on posts.user_id = users.id
                where threads.category_id = :category_id order by posts.date DESC  LIMIT :perPage OFFSET ((:page - 1) * :perPage)');
        $query->execute([
            ':category_id' => $id,
            ':page' => $page,
            ':perPage' => $perPage
        ]);
        return $query->fetchAll(\PDO::FETCH_ASSOC);
    }
}
