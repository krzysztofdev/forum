<?php

namespace App\Model;

class User extends Model {

    public function update(int $id, array $data, array $acceptedKeys = []) {
        if(!empty($data['password']) && $data['password'] == $data['password2'])
            $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
        else
            unset($data['password']);
        return parent::update($id, $data, $acceptedKeys);
    }

    public function emailExists($email) {
        $query = $this->db->prepare('SELECT id FROM users WHERE email = :email');
        $query->execute([':email'=> $email]);
        return $query->fetch(\PDO::FETCH_COLUMN);
    }

    public function userNameExists($name) {
        $query = $this->db->prepare('SELECT id FROM users WHERE name = :name');
        $query->execute([':name'=> $name]);
        return $query->fetch(\PDO::FETCH_COLUMN);
    }

    public function insert(array $data, array $acceptedKeys = []): bool {
        $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
        $data['group_id'] = DEFAULT_GROUP;
        return parent::insert($data, $acceptedKeys);
    }

    public function edit($id, $data) {
        if(empty($data['oldpassword']) || !password_verify($data['oldpassword'], $this->get($_SESSION['user'])['password']))
            unset($data['password']);
        return $this->update($id, $data, ['name', 'email', 'password']);
    }

    public function getByName($name) {
        $query = $this->db->prepare('SELECT * FROM users WHERE name = :name');
        $query->execute([':name' => $name]);
        return $query->fetch(\PDO::FETCH_ASSOC);
    }

    public function login($data) {
        $user = $this->getByName($data['name']);
        if (!empty($user) && !empty($user['active']) && password_verify($data['password'], $user['password'])) {
            session_regenerate_id(true);
            $_SESSION['user'] = $user['id'];
            return true;
        }
        return false;
    }
}
