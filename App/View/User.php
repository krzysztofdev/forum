<?php

namespace App\View;

class User extends View {

    public function login() {
        $this->render('header');
        $this->render('alert', ['alerts' => $this->alerts]);
        $this->render('user/login');
        $this->render('footer');
    }

}
