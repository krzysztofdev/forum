<?php

namespace App\View;

abstract class View {

    protected $alerts;
    private $lang;

    public function __construct() {
        $this->alerts = (new \App\Model\Alert())->getAlerts();
        $this->lang = require_once 'lang'.DIRECTORY_SEPARATOR.LANG.'.php';
    }

    public function __call($name, $arguments) {
        $this->renderSite(lcfirst((new \ReflectionClass($this))->getShortName()).'/'.$name, $arguments[0] ?? []);
    }

    public function renderSite($filename, $args = []) {
        $this->render('header', $args);
        $this->render('alert', ['alerts' => $this->alerts]);
        $this->render($filename, $args);
        $this->render('pagination', $args);
        $this->render('footer', $args);
    }

    public function render(string $filename, $args = []) {
        foreach ($args as $key => $val)
            ${$key} = $val;
        require('App'.DIRECTORY_SEPARATOR.'templates'.DIRECTORY_SEPARATOR.$filename.'.php');
    }

    public function tr($string) {
        $string = strtoupper($string);
        if(!empty($this->lang[$string]))
            return $this->lang[$string];
        else
            return $string;
    }

    public static function getCurrentUrl(array $paramsToModify = []) {
        $params = $_GET;
        if(!empty($paramsToModify))
            foreach($paramsToModify as $key=>$val)
                $params[$key] = $val;
        $controller = $params['task'];
        $method = $params['action'];
        $id = $params['id'] ?? null;
        unset($params['task'], $params['action'], $params['id']);
        return self::urlGenerator($controller, $method, $id, $params);
    }

    public static function urlGenerator(string $controller, string $method, int $id = null, array $params = []) {
        $url = $controller . '/' . $method . '/' . ($id ?? null);
        if(!empty($params)) {
            $url .= '?';
            foreach($params as $key => $val)
                $url .= $key.'='.$val.'&';
            $url = substr($url, 0, -1);
        }
        return self::generateUrl($url);
    }

    private static function generateUrl($address = '') {
        return dirname($_SERVER['PHP_SELF']) . '/' . $address;
    }

    protected function catList($cats, $selected = null, $i = 0) {
        foreach($cats as $cat) {
            echo '<option '.($selected==$cat['id']?'selected ':'').'value="'.$cat['id'].'">'.str_repeat("&nbsp;", $i*4).$cat['name'].'</option>';
            if(!empty($cat['subcats']))
                $this->catList($cat['subcats'], $selected, $i+1);
        }
    }

    protected function br2nl($string) {
        return str_replace('<br>', "\r\n", $string);
    }

}
