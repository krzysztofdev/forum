<?php
$catList = function($cats) use ( &$catList ) {
    $list = '<ul style="list-style-type: none;">';
    foreach($cats as $cat)
        $list .= '<li class="my-3"><a href="'.$this->urlGenerator('category', 'edit', $cat['id']).'">'.$cat['name'].'</a>&nbsp;<small><a href="'.$this->urlGenerator('category', 'remove', $cat['id'], ['xsrf' => \App\Core\AntiCSRF::getToken()]).'" class="text-danger">'.$this->tr('REMOVE').'</a></small>'.(!empty($cat['subcats'])? $catList($cat['subcats']) : '').'</li>';
    return $list.'</ul>';
};
?>
<div class="container">
    <div class="h5"><?=$this->tr('CATEGORIES')?></div>
    <a href="<?= $this->urlGenerator('category', 'add') ?>" class="btn btn-outline-primary btn-sm my-1" role="button"><?=$this->tr('ADD_CATEGORY')?></a>
    <?= $catList($categories) ?>
</div>