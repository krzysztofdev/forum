<div class="container">
    <div class="h5"><?=$this->tr('ADMINISTRATOR_PANEL')?></div>
    <div class="list-group list-group-flush">
        <?php if(\App\Core\Auth::auth()->checkAccess('administrator', 'categories')) { ?><a href="<?= $this->urlGenerator('administrator', 'categories') ?>" class="list-group-item list-group-item-action"><?=$this->tr('CATEGORIES')?></a><?php } ?>
        <?php if(\App\Core\Auth::auth()->checkAccess('group', 'list')) { ?><a href="<?= $this->urlGenerator('group', 'list') ?>" class="list-group-item list-group-item-action"><?=$this->tr('GROUPS')?></a><?php } ?>
        <?php if(\App\Core\Auth::auth()->checkAccess('administrator', 'users')) { ?><a href="<?= $this->urlGenerator('administrator', 'users') ?>" class="list-group-item list-group-item-action"><?=$this->tr('USERS')?></a><?php } ?>
        <?php if(\App\Core\Auth::auth()->checkAccess('route', 'list')) { ?><a href="<?= $this->urlGenerator('route', 'list') ?>" class="list-group-item list-group-item-action"><?=$this->tr('PERMISSIONS')?></a><?php } ?>
    </div>
</div>