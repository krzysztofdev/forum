<div class="container">
    <div class="h5"><?=$this->tr('USER_EDITION')?></div>
    <form method="post" action="">
        <div class="form-group">
            <label for="name"><?=$this->tr('LOGIN')?></label>
            <input class="form-control" value="<?= $user['name'] ?>" placeholder="<?=$this->tr('LOGIN')?>" type="text" id="name" name="data[name]">
        </div>
        <div class="form-group">
            <label for="email"><?=$this->tr('EMAIL')?></label>
            <input class="form-control" value="<?= $user['email'] ?>" placeholder="<?=$this->tr('EMAIL')?>" type="text" id="email" name="data[email]">
        </div>
        <div class="form-group">
            <label for="password"><?=$this->tr('NEW_PASSWORD')?></label>
            <input class="form-control" placeholder="<?=$this->tr('NEW_PASSWORD')?>" type="password" id="password" name="data[password]">
        </div>
        <div class="form-group">
            <label for="password2"><?=$this->tr('REPEAT_PASSWORD')?></label>
            <input class="form-control" placeholder="<?=$this->tr('REPEAT_PASSWORD')?>" type="password" id="password2" name="data[password2]">
        </div>
        <div class="form-group">
            <label for="group"><?=$this->tr('GROUP')?></label>
            <select class="form-control" name="data[group_id]" id="group">
                <?php foreach($groups as $group):?>
                <option value="<?=$group['id']?>"<?=($group['id']==$user['group_id']?'selected':'')?>><?=$group['name']?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-check">
            <input type="checkbox" name="data[active]"  <?= !empty($user['active'])? 'checked' : '' ?> id="active" class="form-check-input">
            <label for="active" class="form-check-label">&nbsp;<?=$this->tr('ACTIVE')?></label>
        </div>
        <input type="hidden" name="xsrf" value="<?=\App\Core\AntiCSRF::getToken()?>">
        <input type="submit" class="btn btn-primary" value="<?=$this->tr('EDIT')?>">
    </form>
</div>