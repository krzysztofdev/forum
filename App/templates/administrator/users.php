<div class="container">
<div class="h5"><?=$this->tr('USERS')?></div>
    <table class="table table-sm table-hover">
        <thead>
            <tr>
            <th scope="col">ID</th>
            <th><?=$this->tr('LOGIN')?></th>
            <th><?=$this->tr('EMAIL')?></th>
            <th><?=$this->tr('ACTIVE')?></th>
            <th><?=$this->tr('GROUP')?></th>
            <th><?=$this->tr('DATE')?></th>
            <th><?=$this->tr('ACTIONS')?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($users as $user) : ?>
            <tr>
            <th scope="row"><?=$user['id']?></th>
            <td><?=$user['name']?></td>
            <td><?=$user['email']?></td>
            <td><?= !empty($user['active'])? $this->tr('YES') : $this->tr('NO') ?></td>
            <td><?=$user['group']?></td>
            <td><?=substr($user['date'],0,19)?></td>
            <td><small><a href="<?=$this->urlGenerator('user', 'manage', $user['id'])?>"><?=$this->tr('EDIT')?></a>&nbsp;<a href="<?=$this->urlGenerator('administrator', 'user', $user['id'], ['xsrf' => \App\Core\AntiCSRF::getToken()])?>" class="text-danger"><?=$this->tr('REMOVE')?></a></small></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
        </table>
</div>