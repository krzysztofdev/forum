<?php if (!empty($alerts)) :
    foreach ($alerts as $type => $messages):
        foreach ($messages as $message) :
            ?>
            <div class="alert alert-<?= $type ?>" role="alert">
            <div class="container">
                            <?=$this->tr($message)?>
              </div>
        </div>
            <?php
        endforeach;
    endforeach;
endif; ?>

