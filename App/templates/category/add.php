<div class="container">
    <div class="h5"><?= $this->tr('ADD_CATEGORY') ?></div>
    <form method="post" action="">
        <div class="form-group">
            <label for="name"><?=$this->tr('NAME')?></label>
            <input class="form-control" type="text" id="name" name="data[name]" placeholder="<?=$this->tr('NAME')?>">
            <label for="category"><?=$this->tr('SUBCATEGORY')?></label>
            <select id="category" name="data[parent_id]" class="form-control form-control-sm">
                <option value="">--&nbsp;<?=$this->tr('MAIN')?>&nbsp;--</option>
                <?=$this->catList($categories, $category['parent_id'])?>
            </select>
        </div>
        <input type="hidden" name="xsrf" value="<?=\App\Core\AntiCSRF::getToken()?>">
        <input type="submit" class="btn btn-primary" value="<?=$this->tr('ADD')?>">
    </form>
</div>