<div class="container">
    <div class="h5"><?= $this->tr('CATEGORY_EDITION') ?></div>
    <form method="post" action="">
        <div class="form-group">
            <label for="name"><?=$this->tr('NAME')?></label>
            <input class="form-control" type="text" id="name" name="data[name]" value="<?=$category['name']?>">
            <label for="category"><?=$this->tr('SUBCATEGORY')?></label>
            <select id="category" name="data[parent_id]" class="form-control form-control-sm">
                <option value="">--&nbsp;<?=$this->tr('MAIN')?>&nbsp;--</option>
                <?=$this->catList($categories, $category['parent_id'])?>
            </select>
        </div>
        <input type="hidden" name="xsrf" value="<?=\App\Core\AntiCSRF::getToken()?>">
        <input type="submit" class="btn btn-primary" value="<?=$this->tr('EDIT')?>">
    </form>
</div>