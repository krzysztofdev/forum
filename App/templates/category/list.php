<div class="container">
    <table class="table table-borderless table-hover">
                <thead class="border-bottom  bg-light">
                    <tr>
                        <th scope="col" class="w-75"><?=$this->tr('CATEGORIES')?></th>
                        <th scope="col"><?=$this->tr('THREADS')?></th>
                        <th scope="col"><?=$this->tr('LAST_POST')?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (!empty($data)) : foreach ($data as $category): ?>
                            <tr>
                                <td><a href="<?= $this->urlGenerator('thread', 'list', $category['id']) ?>"><?= $category['name'] ?></a></td>
                                        <td><?= $category['threads'] ?></td>
                            <td><?= !empty($category['lastPostUser']) ? $category['lastPostUser'] . ',<br>' . date('j.m, H:i', strtotime($category['lastPostDate'])) : $this->tr('NO_POSTS') ?></td>
                            </tr>
                                <?php endforeach;
                            else: ?>
                            <tr><td><?=$this->tr('NO_CATEGORIES')?></td></tr>
                        <?php endif; ?>
                </tbody>
        </table>
</div>