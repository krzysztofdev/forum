<div class="container">
    <div class="h5"><?= $this->tr('EDIT_GROUP') ?></div>
    <form method="post" action="">
        <div class="form-group">
            <label for="name"><?=$this->tr('NAME')?></label>
            <input class="form-control" type="text" value="<?=$group['name']?>" id="name" name="data[name]" placeholder="<?=$this->tr('NAME')?>">
        </div>
        <input type="hidden" name="xsrf" value="<?=\App\Core\AntiCSRF::getToken()?>">
        <input type="submit" class="btn btn-primary" value="<?=$this->tr('EDIT')?>">
    </form>
</div>