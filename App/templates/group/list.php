<div class="container">
    <a href="<?= $this->urlGenerator('group', 'add')?>" class="btn btn-outline-primary btn-sm my-1" role="button"><?=$this->tr('ADD')?></a>
    <table class="table table-borderless table-hover">
                <thead class="border-bottom  bg-light">
                    <tr>
                        <th scope="col" class="w-75"><?=$this->tr('NAME')?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($groups as $group) :?>
                    <tr><td><a href="<?=$this->urlGenerator('group', 'edit', $group['id'])?>"><?= $group['name'] ?></a>&nbsp;<small><a class="text-danger" href="<?=$this->urlGenerator('group', 'remove', $group['id'], ['xsrf' => \App\Core\AntiCSRF::getToken()])?>"><?= $this->tr('REMOVE') ?></a></small></td></tr>
                    <?php endforeach; ?>
                </tbody>
        </table>
</div>