<!DOCTYPE HTML>
<html lang="pl" class="h-100">
    <head>
        <meta charset="utf-8">
        <title>Forum</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    </head>
    <body class="d-flex flex-column h-100">
        <div class="flex-shrink-0">
            <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
                <a class="navbar-brand" href="<?= $this->generateUrl() ?>">Forum</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
                    <div class="navbar-nav">
                        <?php if (empty($_SESSION['user'])) { ?>
                            <a class="nav-item nav-link" href="<?= $this->urlGenerator('user', 'login') ?>"><?=$this->tr('LOG_IN')?></a>
                        <?php if(\App\Core\Auth::auth()->checkAccess('user', 'register')) { ?><a class="nav-item nav-link" href="<?= $this->urlGenerator('user', 'register') ?>"><?=$this->tr('REGISTER')?></a><?php } ?>
                        <?php } else { ?>
                        <?php if(\App\Core\Auth::auth()->checkAccess('administrator', 'panel')) { ?><a class="nav-item nav-link" href="<?= $this->urlGenerator('administrator', 'panel') ?>"><?=$this->tr('ADMINISTRATOR_PANEL')?></a><?php } ?>
                        <?php if(\App\Core\Auth::auth()->checkAccess('user', 'panel')) { ?><a class="nav-item nav-link" href="<?= $this->urlGenerator('user', 'panel') ?>"><?=$this->tr('USER_PANEL')?></a><?php } ?>
                            <a class="nav-item nav-link" href="<?= $this->urlGenerator('user', 'logout') ?>"><?=$this->tr('LOG_OUT')?></a>
                        <?php } ?>
                    </div>
                </div>
            </nav>