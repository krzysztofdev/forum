<?php
if(empty($pagination) || $pagination['items'] == 0)
    return;
?>
<div class="container">
    <ul class="pagination justify-content-center">
<?php
$pages = ceil($pagination['items']/$pagination['perPage']);
    if($pagination['currentPage']>1 && $pages != 1) {
        echo '<li class="page-item"><a class="page-link" href="'.$this->getCurrentUrl(['page'=>$pagination['currentPage']-1]).'">&larr; '.$this->tr('PREV_PAGE').'</a></li>';
    }

    if($pages > 7) {
        if($pagination['currentPage'] > 4 && $pagination['currentPage'] < $pages - 3) {
            echo '<li class="page-item"><a class="page-link" href="'.$this->getCurrentUrl(['page'=>1]).'">1</a></li>&nbsp;<li class="page-item disabled"><span class="page-link">&hellip;</span></li> ';
			foreach(range($pagination['currentPage']-2, $pagination['currentPage']+2) as $i)
				echo '<li class="page-item '.($i == $pagination['currentPage'] ? 'active' : '').'"><a class="page-link" href="'.$this->getCurrentUrl(['page'=>$i]).'">'.$i.'</a></li> ';
            echo '<li class="page-item disabled"><span class="page-link">&hellip;</span></li>&nbsp;<li class="page-item"><a class="page-link" href="'.$this->getCurrentUrl(['page'=>$pages]).'">'.$pages.'</a></li>';
        } else if($pagination['currentPage'] <= 4) {
			foreach(range(1, 6) as $i)
				echo '<li class="page-item '.($i == $pagination['currentPage'] ? 'active' : '').'"><a class="page-link" href="'.$this->getCurrentUrl(['page'=>$i]).'">'.$i.'</a></li> ';
            echo '<li class="page-item disabled"><span class="page-link">&hellip;</span></li>&nbsp;<li class="page-item"><a class="page-link" href="'.$this->getCurrentUrl(['page'=>$pages]).'">'.$pages.'</a></li>';
        } else if($pagination['currentPage'] >= $pages - 3) {
            echo '<li class="page-item"><a class="page-link" href="'.$this->getCurrentUrl(['page'=>1]).'">1</a></li>&nbsp;<li class="page-item disabled"><span class="page-link">&hellip;</span></li> ';
			foreach(range($pages-5, $pages) as $i)
				echo '<li class="page-item '.($i == $pagination['currentPage'] ? 'active' : '').'"><a class="page-link" href="'.$this->getCurrentUrl(['page'=>$i]).'">'.$i.'</a></li> ';
        }
    } else {
        foreach(range(1, $pages) as $i)
            echo '<li class="page-item '.($i == $pagination['currentPage'] ? 'active' : '').'"><a class="page-link" href="'.$this->getCurrentUrl(['page'=>$i]).'">'.$i.'</a></li> ';
    }

    if($pagination['currentPage']<$pages) {
        echo '<li class="page-item"><a class="page-link" href="'.$this->getCurrentUrl(['page'=>$pagination['currentPage']+1]).'">'.$this->tr('NEXT_PAGE').' &rarr;</a></li> ';
    }
?>
    </ul>
</div>
