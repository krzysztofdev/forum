<div class="container">
    <div class="h5"><?= $thread['name'] ?></div>
    <form method="post" action="">
        <div class="form-group">
            <label for="reply"><?=$this->tr('EDIT_REPLY')?></label>
            <textarea class="form-control" id="reply" name="data[content]" rows="10"><?=$this->br2nl($post['content'])?></textarea>
        </div>
        <input type="hidden" name="xsrf" value="<?=\App\Core\AntiCSRF::getToken()?>">
        <input type="submit" class="btn btn-primary" value="<?=$this->tr('EDIT')?>">
    </form>
</div>