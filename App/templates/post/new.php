<div class="container">
    <div class="h5"><?= $thread['name'] ?></div>
    <form method="post" action="">
        <div class="form-group">
            <label for="reply"><?=$this->tr('NEW_REPLY')?></label>
            <textarea class="form-control" id="reply" name="data[content]" rows="10"></textarea>
            <input type="hidden" name="xsrf" value="<?=\App\Core\AntiCSRF::getToken()?>">
            <input type="submit" class="btn btn-primary" value="<?=$this->tr('SUBMIT')?>">
        </div>
    </form>
</div>