<div class="container">
    <div class="h5"><?= $this->tr('ADD_ROUTE') ?></div>
    <form method="post" action="">
        <div class="form-group">
            <label for="controller"><?=$this->tr('CONTROLLER')?></label>
            <input class="form-control" type="text" id="controller" name="data[controller]" placeholder="<?=$this->tr('CONTROLLER')?>">
            <label for="method"><?=$this->tr('METHOD')?></label>
            <input class="form-control" type="text" id="method" name="data[method]" placeholder="<?=$this->tr('METHOD')?>">
        </div>
        <input type="hidden" name="xsrf" value="<?=\App\Core\AntiCSRF::getToken()?>">
        <input type="submit" class="btn btn-primary" value="<?=$this->tr('ADD')?>">
    </form>
</div>