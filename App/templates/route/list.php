<div class="container">
    <a href="<?= $this->urlGenerator('route', 'add') ?>" class="btn btn-outline-primary btn-sm my-1" role="button"><?=$this->tr('ADD_ROUTE')?></a><br>
    <?php foreach ($routes as $route) : ?>
        <a href="<?= $this->urlGenerator('route', 'permissions', $route['id'])?>"><?= $route['controller'].'/'.$route['method']?></a>
        <small><a href="<?= $this->urlGenerator('route', 'remove', $route['id'], ['xsrf' => \App\Core\AntiCSRF::getToken()])?>" class="text-danger"><?=$this->tr('REMOVE')?>></a></small><br>
    <?php endforeach; ?>
</div>