<div class="container">
    <div class="h5"><?=$route['controller'].'/'.$route['method']?></div>
    <form method="post" action="">
        <?php foreach($groups as $group) : ?>
        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="data[<?=$group['id']?>]" value="1" <?= in_array($group['id'], $allowedGroups) ? 'checked': ''?>>
                <?=$group['name']?>
            </label>
        </div>
        <?php endforeach; ?>
        <div class="form-group">
            <input type="hidden" name="xsrf" value="<?=\App\Core\AntiCSRF::getToken()?>">
            <input type="submit" class="btn btn-primary" name="submit" value="<?= $this->tr('SAVE') ?>">
        </div>
    </form>
</div>
