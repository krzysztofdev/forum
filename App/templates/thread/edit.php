<div class="container">
    <div class="h5"><?=$this->tr('EDIT_THREAD')?></div>
    <form method="post" action="">
        <div class="form-group">
            <label for="title"><?=$this->tr('TITLE')?></label>
            <input class="form-control" placeholder="<?=$this->tr('TITLE')?>" type="text" id="title" value="<?=$thread['name']?>" name="data[name]">
            <label for="category"><?=$this->tr('CATEGORY')?></label>
            <select id="category" name="data[category_id]" class="form-control form-control-sm">
                <?= $this->catList($cats, $thread['category_id'])?>
            </select>
            <input type="hidden" name="data[thread_id]" value="<?= $thread['id'] ?>">
        </div>
        <input type="hidden" name="xsrf" value="<?=\App\Core\AntiCSRF::getToken()?>">
    <input type="submit" class="btn btn-primary" value="<?=$this->tr('EDIT')?>">
    </form>
    <br>
</div>