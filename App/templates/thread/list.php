<div class="container">
    <?php if(\App\Core\Auth::auth()->checkAccess('thread', 'new')) { ?><a href="<?= $this->urlGenerator('thread', 'new', $_GET['id']) ?>" class="btn btn-outline-primary btn-sm my-1" role="button"><?=$this->tr('NEW_THREAD')?></a><?php } ?>
    <?php if (!empty($categories)): ?>
            <table class="table table-borderless table-hover">
                <thead class="border-bottom  bg-light">
                    <tr>
                        <th scope="col" class="w-75"><?=$this->tr('CATEGORIES')?></th>
                        <th scope="col"><?=$this->tr('THREADS')?></th>
                        <th scope="col"><?=$this->tr('LAST_POST')?></th>
                    </tr>
                </thead>
                <tbody>
                        <?php foreach ($categories as $category): ?>
                            <tr>
                                        <td><a href="<?= $category['id'] ?>"><?= $category['name'] ?></a></td>
                                        <td><?= $category['threads'] ?></td>
                                    <td><?= !empty($category['lastPostUser']) ? $category['lastPostUser'] . ',<br>' . date('j.m, H:i', strtotime($category['lastPostDate'])) : $this->tr('NO_POSTS') ?></td>
                                    </tr>
                            <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif;
            if (!empty($threads)): ?>
                <table class="table table-borderless table-hover">
                <thead class="border-bottom  bg-light">
                    <tr>
                        <th scope="col" class="w-75"><?=$this->tr('THREADS')?></th>
                        <th scope="col"><?=$this->tr('POSTS')?></th>
                        <th scope="col"><?=$this->tr('LAST_POST')?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($threads as $thread): ?>
                        <tr>
                                    <td><a href="<?= $this->urlGenerator('thread', 'show', $thread['id']) ?>"><?= $thread['name'] ?></a></td>
                                    <td><?= $thread['postsCount'] ?></td>
                                    <td><?= !empty($thread['lastPostUser']) ? $thread['lastPostUser'] . ',<br>' . date('j.m, H:i', strtotime($thread['lastPostDate'])) : $this->tr('NO_POSTS') ?></td>
                                </tr>
                        <?php endforeach; ?>
                </tbody>
                </table>
        <?php else: ?>
    <div class="alert alert-light text-center"><?=$this->tr('NO_THREADS')?></div>
    <?php endif; ?>

</div>