<div class="container">
    <div class="h5"><?=$this->tr('NEW_THREAD')?></div>
    <form method="post" action="">
        <div class="form-group">
            <label for="title"><?=$this->tr('TITLE')?></label>
            <input class="form-control" placeholder="<?=$this->tr('TITLE')?>" type="text" id="title" name="data[name]">
            <label for="reply"><?=$this->tr('CONTENT')?></label>
            <textarea class="form-control" placeholder="<?=$this->tr('CONTENT')?>" id="reply" name="data[content]" rows="10"></textarea>
            <input type="hidden" name="data[category_id]" value="<?= $_GET['id'] ?>">
            <input type="hidden" name="xsrf" value="<?=\App\Core\AntiCSRF::getToken()?>">
            <input type="submit" class="btn btn-primary">
        </div>
    </form>
</div>