<div class="container">
    <h1 class="display-4"><?= $thread['name'] ?></h1>
    <?php if(\App\Core\Auth::auth()->checkAccess('post', 'new')) { ?><a href="<?= $this->urlGenerator('post', 'new', $thread['id']) ?>" class="btn btn-outline-primary btn-sm my-1" role="button"><?=$this->tr('NEW_REPLY')?></a><?php } ?>
    <?php if(\App\Core\Auth::auth()->checkAccess('thread', 'manage')) { ?><a href="<?= $this->urlGenerator('thread', 'manage', $thread['id']) ?>" class="btn btn-outline-primary btn-sm my-1" role="button"><?=$this->tr('EDIT_THREAD')?></a><?php }
    elseif(\App\Core\Auth::auth()->checkAccess('thread', 'edit') && $thread['user_id'] == \App\Core\User::user()->getID()) { ?><a href="<?= $this->urlGenerator('thread', 'edit', $thread['id']) ?>" class="btn btn-outline-primary btn-sm my-1" role="button"><?=$this->tr('EDIT_THREAD')?></a><?php } ?>
    <?php if(\App\Core\Auth::auth()->checkAccess('thread', 'delete')) { ?><a href="<?= $this->urlGenerator('thread', 'delete', $thread['id'], ['xsrf' => \App\Core\AntiCSRF::getToken()]) ?>" class="btn btn-outline-danger btn-sm my-1" role="button"><?=$this->tr('REMOVE_THREAD')?></a><?php }
    elseif(\App\Core\Auth::auth()->checkAccess('thread', 'remove') && $thread['user_id'] == \App\Core\User::user()->getID()) { ?><a href="<?= $this->urlGenerator('thread', 'remove', $thread['id'], ['xsrf' => \App\Core\AntiCSRF::getToken()]) ?>" class="btn btn-outline-danger btn-sm my-1" role="button"><?=$this->tr('REMOVE_THREAD')?></a><?php } ?>
    <?php if (!empty($posts)) foreach ($posts as $post) : ?>
            <div class="row border-top  py-3">
            <div class="col-lg-9">
                <small class="text-muted"><?= $this->tr('ADDED') ?>: <?= date("d-m-Y, H:i", strtotime($post['date'])); ?></small>
                <small>
                    <?php if(\App\Core\Auth::auth()->checkAccess('post', 'manage')) { ?><a href="<?= $this->urlGenerator('post', 'manage', $post['id']) ?>"><?= $this->tr('EDIT') ?></a><?php }
                    elseif(\App\Core\Auth::auth()->checkAccess('post', 'edit') && $post['user_id'] == \App\Core\User::user()->getID()) { ?><a href="<?= $this->urlGenerator('post', 'edit', $post['id']) ?>"><?= $this->tr('EDIT') ?></a><?php } ?>
                    <?php if(\App\Core\Auth::auth()->checkAccess('post', 'delete')) { ?><a href="<?= $this->urlGenerator('post', 'delete', $post['id'] , ['xsrf' => \App\Core\AntiCSRF::getToken()]) ?>" class="text-danger"><?= $this->tr('REMOVE') ?></a><?php }
                    elseif(\App\Core\Auth::auth()->checkAccess('post', 'remove') && $post['user_id'] == \App\Core\User::user()->getID()) { ?><a href="<?= $this->urlGenerator('post', 'remove', $post['id'], ['xsrf' => \App\Core\AntiCSRF::getToken()]) ?>" class="text-danger"><?= $this->tr('REMOVE') ?></a><?php } ?>
                </small>
                <p class="mt-1 overflow-hidden"><?= $post['content'] ?></p>
            </div>
            <div class="col-lg-3 text-center">
                <div class="d-flex flex-column align-items-center">
                    <img class="rounded-circle mb-1" alt="" src="https://www.gravatar.com/avatar/<?= md5(strtolower(trim($post['userEmail']))) ?>?d=robohash" />
                            <div class="mb-1"><?= $post['userName']; ?></div>
                            <div class="badge badge-primary mb-1"><?= $post['groupName']; ?></div>
                            <small class="text-muted mb-1"><?=$this->tr('JOINED')?><br><?= date("d-m-Y", strtotime($post['joinDate'])); ?></small>
                    </div>
            </div>
            </div>
    <?php endforeach; ?>
</div>