<div class="container">
    <div class="h5"><?=$this->tr('USER_EDITION')?></div>
    <form method="post" action="">
        <div class="form-group">
            <label for="name"><?=$this->tr('LOGIN')?></label>
            <input class="form-control" value="<?= $user['name'] ?>" placeholder="<?=$this->tr('LOGIN')?>" type="text" id="name" name="data[name]">
            <label for="email"><?=$this->tr('EMAIL')?></label>
            <input class="form-control" value="<?= $user['email'] ?>" placeholder="<?=$this->tr('EMAIL')?>" type="text" id="email" name="data[email]">
            <label for="oldpassword"><?=$this->tr('OLD_PASSWORD')?></label>
            <input class="form-control" placeholder="<?=$this->tr('OLD_PASSWORD')?>" type="password" id="oldpassword" name="data[oldpassword]">
            <label for="password"><?=$this->tr('NEW_PASSWORD')?></label>
            <input class="form-control" placeholder="<?=$this->tr('NEW_PASSWORD')?>" type="password" id="password" name="data[password]">
            <label for="password2"><?=$this->tr('REPEAT_PASSWORD')?></label>
            <input class="form-control" placeholder="<?=$this->tr('REPEAT_PASSWORD')?>" type="password" id="password2" name="data[password2]">
            <input type="hidden" name="xsrf" value="<?=\App\Core\AntiCSRF::getToken()?>">
            <input type="submit" class="btn btn-primary" value="<?=$this->tr('EDIT')?>">
        </div>
    </form>
</div>