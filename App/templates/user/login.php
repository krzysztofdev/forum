<div class="d-flex justify-content-center">
    <form class="form-signin" method="post" action="">
        <h1 class="h3 mb-3 font-weight-normal"><?=$this->tr('LOG_IN')?></h1>
        <label for="inputLogin" class="sr-only"><?=$this->tr('LOGIN')?></label>
        <input type="text" name="data[name]" id="inputLogin" class="form-control" placeholder="<?=$this->tr('LOGIN')?>" required autofocus>
        <label for="inputPassword" class="sr-only"><?=$this->tr('PASSWORD')?></label>
        <input type="password" name="data[password]" id="inputPassword" class="form-control" placeholder="<?=$this->tr('PASSWORD')?>" required>
        <input type="hidden" name="xsrf" value="<?=\App\Core\AntiCSRF::getToken()?>">
        <button class="btn mt-3 btn-lg btn-primary btn-block" type="submit"><?=$this->tr('LOG_IN')?></button>
    </form>
</div>