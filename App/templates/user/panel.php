<div class="container">
    <div class="h5"><?=$this->tr('USER_PANEL')?></div>
    <div class="list-group list-group-flush">
        <?php if(\App\Core\Auth::auth()->checkAccess('user', 'edit')) { ?><a href="<?= $this->urlGenerator('user', 'edit') ?>" class="list-group-item list-group-item-action"><?=$this->tr('EDIT_PROFILE')?></a><?php } ?>
        <?php if(\App\Core\Auth::auth()->checkAccess('user', 'remove')) { ?><a href="<?= $this->urlGenerator('user', 'remove', null, ['xsrf' => \App\Core\AntiCSRF::getToken()]) ?>" class="list-group-item list-group-item-action text-danger"><?= $this->tr('REMOVE_ACCOUNT') ?></a><?php } ?>
    </div>
</div>