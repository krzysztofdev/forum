<div class="container">
    <div class="h5"><?=$this->tr('REGISTRATION')?></div>
    <form method="post" action="">
        <div class="form-group">
            <label for="login"><?=$this->tr('LOGIN')?></label>
            <input class="form-control" placeholder="<?=$this->tr('LOGIN')?>" type="text" id="login" name="data[name]">
            <label for="email"><?=$this->tr('EMAIL')?></label>
            <input class="form-control" placeholder="<?=$this->tr('EMAIL')?>" type="text" id="email" name="data[email]">
            <label for="password"><?=$this->tr('PASSWORD')?></label>
            <input class="form-control" placeholder="<?=$this->tr('PASSWORD')?>" type="password" id="password" name="data[password]">
            <label for="password2"><?=$this->tr('REPEAT_PASSWORD')?></label>
            <input class="form-control" placeholder="<?=$this->tr('REPEAT_PASSWORD')?>" type="password" id="password2" name="data[password2]">
        </div>
        <input type="hidden" name="xsrf" value="<?=\App\Core\AntiCSRF::getToken()?>">
        <input type="submit" class="btn btn-primary" value="<?=$this->tr('REGISTER')?>">
    </form>
</div>