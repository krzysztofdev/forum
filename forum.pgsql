SET client_encoding = 'UTF8';

CREATE TABLE public.allowed_routes (
    group_id integer NOT NULL,
    route_id integer NOT NULL
);

CREATE TABLE public.categories (
    id integer NOT NULL,
    parent_id integer,
    name character varying(255)
);

CREATE SEQUENCE public.categories_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.categories_id_seq OWNED BY public.categories.id;

CREATE TABLE public.groups (
    id integer NOT NULL,
    name character varying(255)
);

CREATE SEQUENCE public.groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.groups_id_seq OWNED BY public.groups.id;

CREATE TABLE public.posts (
    id integer NOT NULL,
    thread_id integer NOT NULL,
    user_id integer NOT NULL,
    date timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    content character varying(8192)
);

CREATE SEQUENCE public.posts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.posts_id_seq OWNED BY public.posts.id;

CREATE TABLE public.routes (
    id integer NOT NULL,
    controller character varying(255),
    method character varying(255)
);

CREATE SEQUENCE public.routes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.routes_id_seq OWNED BY public.routes.id;

CREATE TABLE public.threads (
    id integer NOT NULL,
    category_id integer NOT NULL,
    user_id integer NOT NULL,
    date timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    name character varying(255)
);

CREATE SEQUENCE public.threads_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.threads_id_seq OWNED BY public.threads.id;

CREATE TABLE public.users (
    id integer NOT NULL,
    group_id integer NOT NULL,
    name character varying(255),
    email character varying(255),
    password character varying(255),
    date timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    active boolean DEFAULT false NOT NULL
);

CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;

ALTER TABLE ONLY public.categories ALTER COLUMN id SET DEFAULT nextval('public.categories_id_seq'::regclass);

ALTER TABLE ONLY public.groups ALTER COLUMN id SET DEFAULT nextval('public.groups_id_seq'::regclass);

ALTER TABLE ONLY public.posts ALTER COLUMN id SET DEFAULT nextval('public.posts_id_seq'::regclass);

ALTER TABLE ONLY public.routes ALTER COLUMN id SET DEFAULT nextval('public.routes_id_seq'::regclass);

ALTER TABLE ONLY public.threads ALTER COLUMN id SET DEFAULT nextval('public.threads_id_seq'::regclass);

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);

COPY public.allowed_routes (group_id, route_id) FROM stdin;
0	32
1	32
2	32
3	32
6	32
1	33
2	33
1	34
2	34
1	15
2	15
1	14
2	14
1	13
2	13
1	27
2	27
1	26
2	26
1	25
2	25
1	8
1	7
1	6
0	29
1	18
1	19
1	20
1	21
1	12
1	11
1	10
1	9
1	3
1	2
1	1
1	4
0	5
1	5
2	5
0	23
1	23
2	23
0	24
1	24
2	24
1	30
1	16
1	28
1	22
1	17
0	35
1	31
2	31
\.

COPY public.groups (id, name) FROM stdin;
0	Unregistred
1	Administratorzy
2	Użytkownicy
\.

COPY public.routes (id, controller, method) FROM stdin;
1	administrator	panel
2	administrator	users
3	administrator	user
4	administrator	categories
5	category	list
6	category	add
7	category	edit
8	category	remove
9	group	list
10	group	edit
11	group	add
12	group	remove
13	post	new
14	post	edit
15	post	remove
16	post	delete
17	post	manage
18	route	list
19	route	add
20	route	remove
21	route	permissions
22	thread	manage
23	thread	list
24	thread	show
25	thread	new
26	thread	remove
27	thread	edit
28	thread	delete
29	user	login
30	user	manage
31	user	remove
32	user	logout
33	user	panel
34	user	edit
35	user	register
\.

COPY public.users (id, group_id, name, email, password, date, active) FROM stdin;
1	1	admin	admin@admin.pl	$2y$10$54uY256V.lgo.xdUMj6MbOYuGwhpOFdJyNrK8e9w6KvxBUnUyY3HS	2000-01-01 00:00:00	t
2	2	user	user@user.pl	$2y$10$dsHbRNKZ1rcT35AtSExYl.qLkOYfvOqnN45vgtt149m76S61oZ/0u	2000-01-01 00:00:00	t
\.


SELECT pg_catalog.setval('public.categories_id_seq', 1, true);

SELECT pg_catalog.setval('public.groups_id_seq', 3, true);

SELECT pg_catalog.setval('public.posts_id_seq', 1, true);

SELECT pg_catalog.setval('public.routes_id_seq', 35, true);

SELECT pg_catalog.setval('public.threads_id_seq', 1, true);

SELECT pg_catalog.setval('public.users_id_seq', 3, true);

ALTER TABLE ONLY public.allowed_routes
    ADD CONSTRAINT allowed_routes_pkey PRIMARY KEY (group_id, route_id);

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.posts
    ADD CONSTRAINT posts_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.routes
    ADD CONSTRAINT routes_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.threads
    ADD CONSTRAINT threads_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.routes
    ADD CONSTRAINT uq_route UNIQUE (controller, method);

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.allowed_routes
    ADD CONSTRAINT allowed_routes_group_id_fkey FOREIGN KEY (group_id) REFERENCES public.groups(id);

ALTER TABLE ONLY public.allowed_routes
    ADD CONSTRAINT allowed_routes_route_id_fkey FOREIGN KEY (route_id) REFERENCES public.routes(id);

ALTER TABLE ONLY public.categories
    ADD CONSTRAINT categories_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES public.categories(id);

ALTER TABLE ONLY public.posts
    ADD CONSTRAINT posts_thread_id_fkey FOREIGN KEY (thread_id) REFERENCES public.threads(id);

ALTER TABLE ONLY public.posts
    ADD CONSTRAINT posts_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);

ALTER TABLE ONLY public.threads
    ADD CONSTRAINT threads_category_id_fkey FOREIGN KEY (category_id) REFERENCES public.categories(id);

ALTER TABLE ONLY public.threads
    ADD CONSTRAINT threads_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_group_id_fkey FOREIGN KEY (group_id) REFERENCES public.groups(id);