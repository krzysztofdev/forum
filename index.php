<?php
use App\Controller\Page;

session_start();
header('Content-Type: text/html; charset=utf-8');
header("X-Frame-Options: DENY");
header("X-Content-Type-Options: nosniff");
require_once('App'.DIRECTORY_SEPARATOR.'Config'.DIRECTORY_SEPARATOR.'config.php');

try {
    $task = ucfirst($_GET['task'] ?? 'Category');
    $controller = 'App\Controller\\' . $task;
    $method = $_GET['action'] ?? 'list';
    if (class_exists($controller)) {
        $controller = new $controller();
        if (method_exists($controller, $method))
            if (\App\Core\Auth::auth()->checkAccess(lcfirst($task), $method))
                $controller->$method();
            else
                (new Page)->forbidden();
        else (new Page)->notFound();
    } else (new Page)->notFound();
} catch (Throwable $e) {
    (new Page)->internalError();
}