# Forum

This is a simple forum which use plain PHP and PostgreSQL database. It is a pre-alpha version, so still there are some missing features which will be hopefully added soon. Currently the only language available is Polish.

## Demo
You can see script in action [here](http://env.ct8.pl/forum/).
Default passwords:  
**admin** : *qwe*  
**user** : *asd*

## Requirements
PHP >= 7.4  
PostgreSQL>= 8.4

## Installation

You have to download via a link at the left panel. After you put the script in the desired destination you have to edit the *App/Config/config.php* file using your database data.